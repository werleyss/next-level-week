import React from 'react';

import whatsappIcon from '../../assets/images/icons/whatsapp.svg';

import './style.css';

function TeacherItem(){
    return (
        <article className="teacher-item">
            <header>
                <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/2444233/avatar.png?width=90" alt="Werley Silva"/>
                <div>
                    <strong>Werley Silva</strong>
                    <span>Fisica</span>
                </div>
            </header>
            <p>
                O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão.
                <br/> <br/>
                O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500, quando uma misturou os caracteres de um texto para criar um espécime de livro
            </p>
            <footer>
                <p>Preço/Hora
                    <strong>R$ 80,00</strong>
                </p>
                <button type="button">
                    <img src={whatsappIcon} alt="Whatsapp"/>
                    Entrar em contato
                </button>
            </footer>
                
        </article>
    )

}

export default TeacherItem;